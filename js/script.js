let saida = document.getElementById('saida');
let txtInicio = document.getElementById('valor-inicial');
let txtFim = document.getElementById('valor-final');

let select = document.getElementById('tipo-trecho');

let btnAciciona = document.getElementById('btn-adiciona');
let btnCalcula = document.getElementById('btn-calcula');
let btnLimpa = document.getElementById('btn-limpar');

let tbResultado = document.getElementById('tabela-resultado');


let asfalto = 1.89;
let chao = 2.21;
let trechos = [];

function adicionaTrecho() {
    let inicio = Number(txtInicio.value);
    let fim = Number(txtFim.value);
    let diferenca = fim - inicio;

    let tipo = select.options[select.selectedIndex].value;

    let valor = (tipo != '2') ? diferenca * asfalto : diferenca * chao;

    const trecho = {
        inicio: inicio,
        fim: fim,
        diferenca: diferenca,
        tipo: tipo,
        valor: valor
    }

    trechos.push(trecho);
    txtInicio.value = txtFim.value;
    txtFim.value = '';
    //console.log(trecho);
}

btnAciciona.addEventListener('click', adicionaTrecho);

function calculaTotal() {
    //tbResultado.innerHTML = '';
    //alert('Calculando os valores');
    let soma = 0.0;
    let contador = 0;
    trechos.map((x) => {
        soma += x.valor;
        tbResultado.innerHTML += `
    <tr>
        <th scope="row">${++contador}</th>
        <td>${x.inicio}</td>
        <td>${x.fim}</td>
        <td>${x.diferenca}</td>
        <td>${(x.tipo != '2') ? 'A' : 'C'}</td>
        <td>${x.valor}</td>
    </tr>`;
    });
    saida.innerHTML += `Total R$ ${soma.toFixed(2)}`;
}

btnCalcula.addEventListener('click', calculaTotal);

function limparValores() {
    console.log(trechos);
}

btnLimpa.addEventListener('click', limparValores);

